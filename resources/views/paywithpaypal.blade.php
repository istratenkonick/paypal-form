<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <title>Paypal Integration</title>
</head>
<body>
<div class="w3-container">
    @if($message = session()->get('success'))
        <div class="w3-panel w3-green w3-display-container">
            <span class="w3-button w3-green w3-large w3-display-topright" onclick="this.parentElement.style.display='none'">
                &times;
            </span>
            <p>{{ $message }}</p>
        </div>
        <?php session()->forget('success'); ?>
    @endif

        @if($message = session()->get('error'))
            <div class="w3-panel w3-green w3-display-container">
            <span class="w3-button w3-green w3-large w3-display-topright" onclick="this.parentElement.style.display='none'">
                &times;
            </span>
                <p>{{ $message }}</p>
            </div>
            <?php session()->forget('error'); ?>
        @endif
    <form class="w3-container w3-display-middle w3-card-4 " method="POST" id="payment-form"  action="{{ route('paypal') }}">
        @csrf
        <h2 class="w3-text-blue">Payment Form</h2>
        <p>Demo PayPal form - Integrating paypal in laravel</p>
        <p>
            <label class="w3-text-blue"><b>Enter Amount</b></label>
            <input class="w3-input w3-border" name="amount" type="text" id="amount"></p>
        <button class="w3-btn w3-blue">Pay with PayPal</button></p>
    </form>
</div>
</body>
</html>
